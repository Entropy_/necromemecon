#!/usr/bin/env python
import zmq
import sys
import threading
import time
from random import randint, random










class ClientTask(threading.Thread):
    """ClientTask"""
    def __init__(self, id):
        self.id = id
        threading.Thread.__init__ (self)

    def run(self):
        context = zmq.Context()
        socket = context.socket(zmq.DEALER)
        identity = u'worker-%d' % self.id
        socket.identity = identity.encode('ascii')
        socket.connect('tcp://127.0.0.1:5570')
        print('Client %s started' % (identity))
        poll = zmq.Poller()
        poll.register(socket, zmq.POLLIN)
        reqs = 0
        while True:
            reqs = reqs + 1
            print('Req #%d sent..' % (reqs))
            socket.send_string(u'request #%d' % (reqs))
            for i in range(5):
                sockets = dict(poll.poll(1000))
                if socket in sockets:
                    # SIGINT will normally raise a KeyboardInterrupt, just like any other Python call
                    try:
                        msg = socket.recv()
                        print('Client %s received: %s' % (identity, msg))
                    except KeyboardInterrupt:
                        print("W: interrupt received, stopping…")
                    finally:
                        # clean up
                        socket.close()
                        context.term()

        socket.close()
        context.term()

client = ClientTask(227)
client.run()
client.join()
