#!/usr/bin/env python

import curses
from curses import ascii
import os
import time
import sys
import fpcc as fpc
import binascii
import threadexplorer
import gnupg
import json
import fthr
import random
import asciified as asc

# Get the avi for the session
#avi = find(username+".txt", '/home/{0}/SCHEDULING-jpeg-png-toascii/avis/'.format(username))
# Get the avi length, this should be static
#avi = open(avi, "r")
#sizeavi = avi.readlines()
#print len(sizeavi)
#for x in range(len(sizeavi)):
#    print sizeavi[x]
# Open the avi for reading
#!/usr/bin/env python
import zmq
import sys
import threading
import time
from random import randint, random
#ZMQ sockets











class ClientTask(threading.Thread):
    """ClientTask"""
    def __init__(self, id):
        username = os.getlogin()
        gpg = gnupg.GPG(gnupghome='/home/{0}/.SCHEDULING/'.format(username), use_agent=True, keyring='ringo',
        secret_keyring='rings')
        # Define our finder function
        # Grab our username for easier walking of the path
        print username
        self.id = id
        threading.Thread.__init__ (self)

    def find(name, path):
        for root, dirs, files in os.walk(path):
            if name in files:
                return os.path.join(root, name)


    def run(self):
        username = os.getlogin()
        context = zmq.Context()
        socket = context.socket(zmq.DEALER)
        insocket = context.socket(zmq.ROUTER)
        identity = u'worker-%d' % self.id
        inidentity = u'clientinput-%d' % self.id
        socket.identity = identity.encode('ascii')
        socket.connect('tcp://127.0.0.1:5570')
        insocket.identity = inidentity.encode('ascii')
        insocket.bind('tcp://127.0.0.1:5571')
        print('Client %s started' % (identity))
        poll = zmq.Poller()
        poll.register(socket, zmq.POLLIN)
        poll.register(insocket, zmq.POLLIN)
        postwriter = PostWriter()
        curses.wrapper(postwriter.run(1))


        reqs = 0
        while True:
            reqs = reqs + 1
            print('Req #%d sent..' % (reqs))
            socket.send_string(u'request #%d' % (reqs))
            for i in range(5):
                sockets = dict(poll.poll(1000))
                if socket in sockets:
                    # SIGINT will normally raise a KeyboardInterrupt, just like any other Python call
                    try:
                        inmsg = insocket.recv()
                        print('Client %s received: %s' % (inidentity, inmsg))
                        if inmsg == "GET":
                            print ("GOT GET REQUEST FROM CLIENT")
                            #DO WORK
                            inmsg = insocket.recv()
                            print str(inmsg)
                        else:
                            print "INVALID DATA"
                    except KeyboardInterrupt:
                        print("W: interrupt received, stopping")
                    finally:
                        # clean up
                        insocket.close()
                        context.term()

                    try:
                        msg = socket.recv()
                        print('Client %s received: %s' % (identity, msg))
                    except KeyboardInterrupt:
                        print("W: interrupt received, stopping")
                    finally:
                        # clean up
                        socket.close()
                        context.term()


        socket.close()
        context.term()


class ThreadWindow():
    def run(init=0, threadpos=[], threadUID=0):

        threadpos = fthr.getpos()
        if len(fthr.getpos()) == 0:
            threadpos.append((1, 1))
        user = 'owlet@treehole.com'
        stdscr = curses.initscr()
        curses.noecho()
        curses.cbreak()

        stdscr.keypad(1)
        # Init the postwriter
        if init == 1:
            context = zmq.Context()
            socket = context.socket(zmq.DEALER)
            identity = u'clientin-%d' % username
            socket.identity = identity.encode('ascii')
            socket.connect('tcp://127.0.0.1:5571')

        # Thread explorer
        stdscr.addstr(37,96, "HOME Peruses a thread")
        stdscr.addstr(39,96, "INSERT creates a new thread")
        stdscr.addstr(41,96, "F12 exits")
        #stdscr.addstr(39,96, "F5   Imports a thread")
        #stdscr.addstr(41,96, "F2   Bans a user from seeing you")
        #stdscr.addstr(43,96, "F3   Shadowbans a user")
        #stdscr.addstr(45,96, "F6   Bans a user from posting in your threads")
        stdscr.refresh()
        threadwin = stdscr.subwin(22, 95, 3, 0)
        threadwin.border()
        threadwin.move(1, 1)
        cursy = 3
        cursx = 1
        threadwin.keypad(1)
        # Show threads
        #thread1 = fpc.concat('DA3986498FFD5777A7F5A273EAAE1660A51A6404')
        #print thread1
        #thread1 = thread1.encode('UTF-8')
        #TODO Get threads from sink.py
        #INitialize if empty
        threadid = fthr.getheaders()
        if len(fthr.countthreads()) == 0:
            threadid = []
            threadid.append('IMA-GHOAST')
        #print threadid
        currentthread = threadpos[0]
        numthreads = fthr.countthreads()
        if len(fthr.countthreads()) == 0:
            numthreads = []
            numthreads.append('IMA-GHOAST')
        threads = fthr.processhead()
        if len(fthr.processhead()) == 0:
            threads = []
            threads = [('IMA-GHOAST')]
        threadUID = threadid[0]
        #if threadUID == 'GHOAST':
        #    stdscr.addstr(0, 2, 'OOOooooOOOOOOooooOOOOO')
        #else:
        #    if len(threadUID) >= 1:
        #        threadUID = str(threadUID).split('-')
        #        threadUID = threadUID[1]
        #    else:
        #        print 'ooooOOOOOoooOimaGHOAST!'
        #print threads
        y = 0
        for x in threads:
            if x != 0:
                threads[y] = binascii.b2a_base64(x)
                y += 1
        y = 0
        #thread1 = binascii.b2a_base64(thread1)
        #threads.append(thread1)
        #threads.append(thread1)



        threadwin.move(1, 1)
        x = 0
        while True:
            if threadUID == 'GHOAST':
                stdscr.addstr(0, 2, 'OOOooooOOOOOOooooOOOOO')
            else:
                if len(threadUID.split('-')) >= 2:
                    threadUID = str(threadUID).split('-')
                    threadUID = threadUID[1]        #print x
            z = 0
            for y in threadpos:
                thy, thx = y
                threadwin.addstr(thy, thx, threads[z])
                z += 1
            z = 0
            ty, tx = threadpos[x]
            threadwin.addstr(ty, tx, threads[x], curses.A_STANDOUT)
            threadwin.move(ty, tx)
            threadwin.border()
            threadwin.refresh()
            #print len(threadpos)
            cursy, cursx = curses.getsyx()

            _ = threadwin.getch()
            if _ == curses.KEY_IC:
                a = gpg.list_keys(user)
                postwriter.postwriter(0, 0, 1, a[1]['fingerprint'], True, threadpos)
            if _ == curses.KEY_BACKSPACE:
                _ = None
            if _ == curses.KEY_ENTER:
                _ = None
            if _ == curses.KEY_HOME:
                #TODO enter the thread
                socket.send_string("GET")
                post.postreader(threadid[x], 1, 'owlet@treehole.com', threadUID)
                #readthread(threadid)
                _ = None
            if _ == curses.KEY_UP:
                _ = None
                cursy, cursx = curses.getsyx()
                cursy = cursy - 3
                cursx = cursx - 1
                if cursy != 0:
                    if x < len(threadpos):
                        x += 1
                        if x >= len(threadpos):
                            x = len(threadpos)-1
                        ty, tx = threadpos[x]
                        currentthread = threadpos[x]
                        threadwin.addstr(ty, tx, threads[x], curses.A_STANDOUT)
                        threadwin.border()
                        threadwin.move(ty, tx)
                        threadwin.refresh()
                        threadUID = threadid[x]
            if _ == curses.KEY_DOWN:
                _ = None
                cursy, cursx = curses.getsyx()
                cursy = cursy - 3
                cursx = cursx - 1
                if cursy != 21:
                    if x < len(threadpos):
                        x -= 1
                        if x < 0:
                            x = 0
                        ty, tx = threadpos[x]
                        threadwin.addstr(ty, tx, threads[x], curses.A_STANDOUT)
                        threadwin.move(ty, tx)
                        threadwin.border()
                        threadwin.refresh()
                        threadUID = threadid[x]
                    else:
                        pass
            if _ == curses.KEY_LEFT:
                _ = None
                cursy, cursx = curses.getsyx()
                cursy = cursy - 3
                cursx = cursx - 1
                if cursx != 0:
                    if x < len(threadpos):
                        x -= 1
                        if x < 0:
                            x = 0
                        ty, tx = threadpos[x]
                        threadwin.addstr(ty, tx, threads[x], curses.A_STANDOUT)
                        threadwin.move(ty, tx)
                        threadwin.border()
                        threadwin.refresh()
                        threadUID = threadid[x]
                    else:
                        pass
            if _ == curses.KEY_RIGHT:
                _ = None
                cursy, cursx = curses.getsyx()
                cursy = cursy - 3
                cursx = cursx - 1
                if cursx != 94:
                    if x < len(threadpos):
                        x += 1
                        if x >= len(threadpos):
                            x = len(threadpos)-1
                        ty, tx = threadpos[x]
                        threadwin.addstr(ty, tx, threadid[x], curses.A_STANDOUT)
                        threadwin.move(ty, tx)
                        threadwin.border()
                        threadwin.refresh()
                        threadUID = threadid[x]
                    else:
                        pass
    #        if _ == curses.KEY_F10:
    #            stdscr.addstr(2, 0, "Returning to Post Editor...")
    #            stdscr.refresh()
    #            #print "Returning to Thread Explorer..."
    #            time.sleep(2)
    #            stdscr.move(2, 0)
    #            stdscr.clrtoeol()
    #            threadwin.border()
    #            stdscr.refresh()
    #            curses.wrapper(postwriter.postwriter(0))
            if _ == curses.KEY_F12:
                stdscr.addstr(2, 0, "Exiting...")
                stdscr.refresh()
                time.sleep(2)
                stdscr.move(2, 0)
                stdscr.clrtoeol()
                stdscr.refresh()
                return
            else:
                #cursy, cursx = curses.getsyx()
                #cursy = cursy - 3
                #cursx = cursx - 1
                #if cursy >= 20:
                    #threadwin.move(cursy-1, cursx)
                    #_ = None
                if _ != None:
                    pass
                    #cursx += 1
                    #threadwin.addch(_)

            thread1 = fpc.concat('DA3986498FFD5777A7F5A273EAAE1660A51A6404')
            #print thread1
            #thread1 = thread1.encode('UTF-8')
            #TODO Get threads from sink.py
            z = 0
    #        thy = 0
    #        thx = 0
    #        for y in threadpos:
    #            thy, thx = y
    #            threadwin.addstr(thy, thx, threads[z])
    #            threadwin.border()
    #            threadwin.refresh()
    #            z += 1
    #        z = 0
            threadwin.addstr(ty, tx, threads[x], curses.A_STANDOUT)
            threadwin.move(ty, tx)
            threadwin.border()
            threadwin.refresh()


        threadwin.keypad(0)
        postwin.keypad(0)
        curses.nocbreak()
        stdscr.keypad(0)
        curses.echo()
        curses.endwin()




class PostWriter(threading.Thread):
    def run(init=0, threadid=0, postnum=0, threadUID=0, newthread=False, threadpos=[], user='owlet@treehole.com'):
        username = os.getlogin()
        #TODO get the threadnumber and postnumber from SOMEWHERE
        threadpos=fthr.getpos()
        cursx = 0
        cursy = 0
        linenum = 0
        stdscr = curses.initscr()
        curses.noecho()
        curses.cbreak()
        stdscr.keypad(1)

        # Define our window size
        begin_x = 0
        begin_y = 0
        height = 60
        width = 237
        win = curses.newwin(height, width, begin_y, begin_x)

        #stdscr.move(3, 32)





        # Postwriter window
        if init == 1:
            threadnum = "DRAFT"
            postnum = "DRAFT"
            stdscr.addstr(linenum, 152, "-==SCHEDULING postwriter==-")
            linenum += 1
            stdscr.addstr(1, 3, "-==SCHEDULING Thread Explorer==-")
            stdscr.addstr(linenum, 135, "-==Posting to thread {0}==--==This is post {1}==-".format(threadnum, postnum))
            linenum += 2
            stdscr.addstr(3, 195, "-==Post Controls==-")
            stdscr.addstr(4, 196, "F8  Save Draft")
            stdscr.addstr(5, 196, "F4  Load Draft")
            stdscr.addstr(6, 196, "F5  Post to thread")
            stdscr.addstr(7, 196, "END Change your avatar")
            stdscr.addstr(8, 196, "F12 Exit")
            # Avatar controls
            avi = find(username+".txt", '/home/{0}/SCHEDULING/avis/'.format(username))
            # Get the avi length, this should be static
            avi = open(avi, "r")
            sizeavi = avi.readlines()
            for i in range(len(sizeavi)-1):
                linenum += 1
                linetoadd = sizeavi[i]
                linetoadd.rstrip('\n')
                print linetoadd
                stdscr.addnstr(linenum, 98, linetoadd, 32)

            avi.close()
            stdscr.refresh()
            #end avi controls
        else:
            #header = open('/home/{0}/SCHEDULING/mod/headers'.format(username), 'r')
            #head = json.load(header)
            #header.close()
            #users, threadUID = head[threadid]
            stdscr.clear()
            stdscr.addstr(0, 152, "-==SCHEDULING postwriter==-")
            stdscr.addstr(1, 3, "-==SCHEDULING Thread Explorer==-")
            stdscr.addstr(1, 135, "-==Posting to thread {0}==--==This is post {1}==-".format(threadUID, postnum))
            stdscr.addstr(3, 195, "-==Post Controls==-")
            stdscr.addstr(4, 196, "F8  Save Draft")
            stdscr.addstr(5, 196, "F4  Load Draft")
            stdscr.addstr(6, 196, "F5  Post to thread")
            stdscr.addstr(7, 196, "END Change your avatar")
            stdscr.addstr(8, 196, "F12 Exit to thread")
            stdscr.refresh()


        if newthread:
            if init == 1:
                context = zmq.Context()
                socket = context.socket(zmq.DEALER)
                identity = u'clientin-%d' % username
                socket.identity = identity.encode('ascii')
                socket.connect('tcp://127.0.0.1:5571')


            header = open('/home/{0}/SCHEDULING/mod/headers'.format(username), 'r')
            head = json.load(header)
            header.close()
            users = 1
            threadnum = len(fthr.countthreads())+1
            while True:
                threadposition = (random.randint(0, 19), random.randint(0, 55))
                if threadposition in threadpos:
                    pass
                else:
                    threadpos.append(threadposition)
                    break
            postnum = 1
            stdscr.addstr(0, 152, "-==SCHEDULING postwriter==-")
            stdscr.addstr(1, 3, "-==SCHEDULING Thread Explorer==-")
            stdscr.addstr(1, 135, "-==Posting to thread {0}==--==This is post {1}==-".format(threadUID, postnum))
            stdscr.addstr(3, 195, "-==Post Controls==-")
            stdscr.addstr(4, 196, "F8  Save Draft")
            stdscr.addstr(5, 196, "F4  Load Draft")
            stdscr.addstr(6, 196, "F5  Post to thread")
            stdscr.addstr(7, 196, "END Change your avatar")
            stdscr.addstr(8, 196, "F12 Exit")
            stdscr.refresh()
            stdscr.addstr(2, 0, "Creating a new thread...")
            stdscr.addstr(2, 25, "Choose a new position for this thread[X]")
            stdscr.refresh()
        else:
            #header = open('/home/{0}/SCHEDULING/mod/headers'.format(username), 'r')
            #head = json.load(header)
            #header.close()
            #users, threadUID = head[threadid]
            stdscr.clear()
            stdscr.addstr(0, 152, "-==SCHEDULING postwriter==-")
            stdscr.addstr(1, 3, "-==SCHEDULING Thread Explorer==-")
            stdscr.addstr(1, 135, "-==Posting to thread {0}==--==This is post {1}==-".format(threadUID, postnum))
            stdscr.addstr(3, 195, "-==Post Controls==-")
            stdscr.addstr(4, 196, "F8  Save Draft")
            stdscr.addstr(5, 196, "F4  Load Draft")
            stdscr.addstr(6, 196, "F5  Post to thread")
            stdscr.addstr(7, 196, "END Change your avatar")
            stdscr.addstr(8, 196, "F12 Exit to thread")
            stdscr.refresh()
        postwin = stdscr.subwin(33, 63, 3, 131)
        postwin.border()
        postwin.refresh()
        stdscr.refresh()
        # Define the thread window
        threadwin = stdscr.subwin(22, 95, 3, 0)
        postwin.move(1, 1)
        cursy = 5
        cursx = 131
        if init == 1:
            threadwindow = ThreadWindow()
            threadwindow.run(1, threadpos, 0)
        postwin.keypad(1)
        while True:
            _ = postwin.getch()

            if _ == curses.KEY_BACKSPACE:
                _ = None
                cursy, cursx = curses.getsyx()
                cursy = cursy - 3
                cursx = cursx - 131
                if cursx != 0 and cursy != 0:
                    postwin.move(cursy, cursx-1)
                    postwin.delch(cursy, cursx-1)
                    postwin.border()
                    postwin.refresh()
                    cursx = cursx-1
                if cursy != 0 and cursx == 0:
                    postwin.move(cursy-1, 0)
                    cursy = cursy - 1
                if cursy == 0 and cursx != 0:
                    postwin.move(cursy, cursx-1)
                    postwin.delch(cursy, cursx-1)
                    postwin.border()
                    postwin.refresh()
                    cursx = cursx-1
                if cursy == 0 and cursx == 0:
                    pass
            if _ == curses.KEY_ENTER:
                cursy, cursx = curses.getsyx()
                cursy = cursy - 3
                cursx = cursx - 131
                if cursy <= 30:
                    cursy = cursy + 1
                if cursy >= 30:
                    postwin.move(cursy-1, 0)
                    _ = None
            if _ == curses.KEY_UP:
                _ = None
                cursy, cursx = curses.getsyx()
                cursy = cursy - 3
                cursx = cursx - 131
                if cursy != 0:
                    cursy = cursy - 1
                    postwin.move(cursy, cursx)
            if _ == curses.KEY_DOWN:
                _ = None
                cursy, cursx = curses.getsyx()
                cursy = cursy - 3
                cursx = cursx - 131
                if cursy != 32:
                    cursy = cursy + 1
                    postwin.move(cursy, cursx)
            if _ == curses.KEY_LEFT:
                _ = None
                cursy, cursx = curses.getsyx()
                cursy = cursy - 3
                cursx = cursx - 131
                if cursx != 0:
                    cursx = cursx - 1
                    postwin.move(cursy, cursx)
            if _ == curses.KEY_RIGHT:
                _ = None
                cursy, cursx = curses.getsyx()
                cursy = cursy - 3
                cursx = cursx - 131
                if cursx != 60:
                    cursx = cursx + 1
                    postwin.move(cursy, cursx)
            if _ == curses.KEY_F5:
                print "Posting!"
                # BEGIN JSON CRAP
                a = gpg.list_keys(user)
                recipients = ['phil@sausage.com', a[1]['fingerprint']]
                header = open('/home/{0}/SCHEDULING/mod/headers'.format(username), 'rw+')
                # ^^ For loading headers
                c = [(x, recipients[x]) for x in range(len(recipients))]
                if newthread:
                    threadUID = str(random.randint(0, 300))+a[1]['fingerprint']
                else:
                    threadUID = str(threadUID)
                #print threadUID
                #print threadUID
                #print threadUID
                #time.sleep(4)
                #entry = [ c, threadnum+1, postnum+1, str(threadUID) ]
                #json.dump(entry, header)
                header.close()
                # ^^ For writing headers, which should be done any time a post is made
                #    Or when a thread is made
                # END JSON CRAP
                if os.path.exists('/home/{0}/SCHEDULING/p/{1}'.format(username, str(threadUID))) == False:
                    os.mkdir('/home/{0}/SCHEDULING/p/{1}'.format(username, str(threadUID)))
                filename = '/home/{0}/SCHEDULING/p/{1}/{2}.TXT'.format(username, str(threadUID), str(postnum)+'-'+str(threadUID))
                postnum += 1
                post = open(filename, 'w+')
                for y in range(36):
                    #print "Downloading downloading downloading..."
                    if y > 2:
                        line = stdscr.instr(y, 98)
                        post.write(line+'\n')
                post.close()
                post = open(filename, 'rb')
                n = 2
                #recipients = users[1::2]
                #print recipients
                recipients = ['phil@sausage.com', 'owlet@treehole.com']
                e = gpg.encrypt_file(post, recipients, output=filename, always_trust=True)
                string = str(e.ok)
                stdscr.addstr(2, 0, string)
                stdscr.refresh()
                time.sleep(1)
                stdscr.move(2, 0)
                stdscr.clrtoeol()
                stdscr.refresh()
                string = str(e.status)
                stdscr.addstr(2, 0, string)
                stdscr.refresh()
                time.sleep(1)
                stdscr.move(2, 0)
                stdscr.clrtoeol()
                stdscr.refresh()
                post.close()
                print filename
                time.sleep(2)
                postwin.clear()
                postwin.refresh()
                postwin.border()
            if _ == curses.KEY_F8:
                _ = None
                stdscr.addstr(2, 16, "Saving draft for thread {0}...".format(threadnum))
                filename = '{0}DRAFT.TXT'.format(threadnum)
                draft = open(filename, 'w+')
                for y in range(33):
                    #print "Downloading downloading downloading..."
                    line = postwin.instr(y, 0)
                    draft.write(line+'\n')
                draft.close
                stdscr.refresh()
                postwin.border()
                postwin.refresh()
                time.sleep(2)
                cursy, cursx = curses.getsyx()
                stdscr.move(2, 0)
                stdscr.clrtoeol()
                stdscr.refresh()
                postwin.move(1, 1)
            if _ == curses.KEY_F4:
                _ = None
                stdscr.addstr(2, 16, "Loading draft for thread {0}".format(threadnum))
                stdscr.refresh()
                time.sleep(2)
                cursy, cursx = curses.getsyx()
                stdscr.move(2, 0)
                stdscr.clrtoeol()
                cursy = 32
                cursx = 131
                stdscr.move(cursy, cursx)
                stdscr.refresh()
                draft = open('/home/{0}/SCHEDULING/{1}DRAFT.TXT'.format(username, threadnum), 'rw+')
                draftin = draft.readlines()
                linenum = 0
                for i in range(len(draftin)-1):
                    linetoadd = draftin[i]
                    linetoadd.rstrip('\n')
                    #print linetoadd
                    postwin.addstr(linenum, 0, linetoadd, 0)
                    linenum += 1
                draft.close()
                postwin.refresh()
                os.remove('/home/{0}/SCHEDULING/{1}DRAFT.TXT'.format(username, threadnum))
            if _ == curses.KEY_END:
                stdscr.addstr(55, 142, "Enter the path of the image:")
                stdscr.addstr(56, 142, "END to finish. HOME to cancel.")
                stdscr.move(57, 142)
                stdscr.refresh()
                while True:
                    _ = stdscr.getch()
                    if _ == curses.KEY_BACKSPACE:
                        _ = None
                        cursy, cursx = curses.getsyx()
                        if cursx != 142 and cursy != 57:
                            stdscr.move(cursy, cursx-1)
                            stdscr.delch(cursy, cursx-1)
                            stdscr.refresh()
                            cursx = cursx-1
                        if cursy != 57 and cursx == 142:
                            stdscr.move(cursy-1, 0)
                            cursy = cursy - 1
                        if cursy == 57 and cursx != 142:
                            stdscr.move(cursy, cursx-1)
                            stdscr.delch(cursy, cursx-1)
                            stdscr.refresh()
                            cursx = cursx-1
                        if cursy == 57 and cursx == 142:
                            pass
                    if _ == curses.KEY_HOME:
                        _ = None
                        stdscr.move(55, 142)
                        stdscr.clrtoeol()
                        stdscr.move(56, 142)
                        stdscr.clrtoeol()
                        stdscr.move(57, 142)
                        stdscr.clrtoeol()
                        stdscr.refresh()
                        avi = None
                        postwin.move(1, 1)
                        break
                    if _ == curses.KEY_END:
                        _ = None
                        filename = stdscr.instr(57, 142)
                        filename = filename.rstrip()
                        #print filename
                        try:
                            avi = asc.asciified(filename)
                            break
                        except:
                            stdscr.addstr(13, 196, "FILE NOT FOUND")
                            time.sleep(5)
                            stdscr.move(57, 142)
                            stdscr.clrtoeol()
                            stdscr.move(57, 142)
                            stdscr.clrtoeol()
                            stdscr.move(57, 142)
                            stdscr.refresh()
                    else:
                        if _ != None:
                            stdscr.addch(_)
                #avi = find(avi+".txt", '/home/{0}/SCHEDULING-jpeg-png-toascii/avis/'.format(username))
                # Get the avi length, this should be static
                if avi != None:
                    avi = open(avi, "r")
                    sizeavi = avi.readlines()
                    linenum = 3
                    for i in range(len(sizeavi)-1):
                        linenum += 1
                        linetoadd = sizeavi[i]
                        linetoadd.rstrip('\n')
                        #print linetoadd
                        stdscr.addnstr(linenum, 98, linetoadd, 32)
                    avi.close()
                    stdscr.move(55, 142)
                    stdscr.clrtoeol()
                    stdscr.move(56, 142)
                    stdscr.clrtoeol()
                    stdscr.move(57, 142)
                    stdscr.clrtoeol()
                    stdscr.refresh()
                    postwin.move(1, 1)
                    stdscr.refresh()
            if _ == curses.KEY_F12:
                stdscr.addstr(2, 129, "Returning...")
                stdscr.refresh()
                #print "Returning to Thread Explorer..."
                time.sleep(2)
                stdscr.move(2, 129)
                stdscr.clrtoeol()
                postwin.border()
                stdscr.refresh()
                threadwindow = ThreadWindow()
                threadwindow.run(1, threadpos, 0)
#                return threadUID
                #curses.wrapper(threadexplorer.threadwindow(0, threadpos, threadUID))
            else:
                cursy, cursx = curses.getsyx()
                cursy = cursy - 3
                cursx = cursx - 131
                if cursy >= 32 and _ != None:
                    postwin.move(cursy-1, cursx)
                    _ = None
                if _ != None:
                    cursx += 1
                    postwin.addch(_)
                    postwin.border()
                    postwin.refresh()




def main():
    time.sleep(.05)
    # Make sure nothing is funky
    def set_shorter_esc_delay_in_os():
        os.environ.setdefault('ESCDELAY', '25')
    set_shorter_esc_delay_in_os()
    client = ClientTask(227)
    client.run()
main()
