#!/usr/bin/env python


import os
import time
import sys

import binascii

import gnupg
import json

import random
import json

username = os.getlogin()
gpg = gnupg.GPG(gnupghome='/home/{0}/.SCHEDULING/'.format(username), use_agent=True, keyring='ringo',
secret_keyring='rings')

threadnum = 0
postnum = 0

# BEGIN JSON CRAP
a = gpg.list_keys('bender@bender.bender')
recipients = ['phil@sausage.com', a[1]['fingerprint']]
header = open('/home/{0}/SCHEDULING-postwriter/headers'.format(username), 'w+')
#head = json.load(header)
jsonhead = open('jsonhead', mode="w+")
json.dump([], jsonhead)
jsonhead.close()
# ^^ For loading headers
c = [(x, recipients[x]) for x in range(len(recipients))]
jsonheaders = open('jsonhead', mode="w+")
entry = [ c, threadnum+1, postnum+1, str(postnum)+str(random.randint(0, 300))+a[1]['fingerprint']]
entry = json.dumps(entry)
header.write(entry)
header.close()
jsonheaders.close()
# ^^ For writing headers, which should be done any time a post is made
#    Or when a thread is made
# END JSON CRAP
