#!/usr/bin/env python
import time
import os
import zmq
import gnupg
inputs = False
#Keys for gnupg
#TODO   Add keyrings for each thread
import os
username = os.getlogin()
gpg = gnupg.GPG(gnupghome='/home/{0}/.SCHEDULING'.format(username), use_agent=True, keyring='ringo',
secret_keyring='rings')
#save for later, see if errors pop up
#gpg.encoding = 'utf-8'

#sockets for ZMQ
context = zmq.Context()
sender = context.socket(zmq.PUSH)
#TODO   figure out how to scan for/distribute addresses
#       I'm thinking there will be nine entrance threads
#       Then when you join one thread, you can see the other
#       threads users in your threads have joined/created
sender.bind("tcp://127.0.0.1:5556")

# Socket to listen for workers on
workers = context.socket(zmq.PULL)
workers.bind("tcp://127.0.0.1:5555")


# Socket with direct access to the sink: used to syncronize
# start of batch
sink = context.socket(zmq.PUSH)
sink.connect("tcp://127.0.0.1:5558")





#TODO   Get workers to signal when ready
print("Press Enter when the workers are ready: ")
numworker = 0
while numworker <= 0:
    inwork = workers.recv()
    if inwork == b'_':
        numworker += 1
        print("%s worker has connected" % (numworker))
_ = raw_input()
print("Sending tasks to workers...")

# The first message is "0" and signals start of batch
sink.send(b'0')
sink.send_string('1')
sink.send_string("10001.txt")
for x in range(1):
    sender.send_string("10001.txt")

#TODO   Assembly of message body

# Give 0MQ time to deliver
time.sleep(1)
#TODO   Find out why this is a bad idea.
