#!/usr/bin/env python
import sys
import os
import json
import curses
import time
import fpcc as fpc
import postwriter
import gnupg
import random
import fthr
username = os.getlogin()
gpg = gnupg.GPG(gnupghome='/home/{0}/.SCHEDULING/'.format(username), use_agent=True, keyring='ringo', secret_keyring='rings')


def postreader(threadid, init=1, user='Bender B Rodriguez', threadUID=0, postnum=1):
    if init == 1:
        context = zmq.Context()
        socket = context.socket(zmq.DEALER)
        identity = u'clientin-%d' % username
        socket.identity = identity.encode('ascii')
        socket.connect('tcp://127.0.0.1:5571')

    stdscr = curses.initscr()
    curses.noecho()
    curses.cbreak()
    stdscr.keypad(1)
    pwin = stdscr.subwin(34, 95, 25, 0)
    pwin.border()
    pwin.refresh()
    cursy, cursx = curses.getsyx()
    pwin.keypad(1)
    # BEGIN JSON CRAP
    a = gpg.list_keys(user)
    recipients = ['phil@sausage.com', a[1]['fingerprint']]
    header = open('/home/{0}/SCHEDULING/mod/headers'.format(username), 'rw+')
    head = json.load(header)
    # ^^ For loading headers
    c = [(x, recipients[x]) for x in range(len(recipients))]
    #jsonheaders = open('jsonhead', mode="w+")
    #entry = [ c, threadnum+1, postnum+1, str(random.randint(0, 300))+a[1]['fingerprint']]
    #entry = json.dumps(entry)
    #header.write(entry)
    header.close()
    #jsonheaders.close()
    # ^^ For writing headers, which should be done any time a post is made
    #    Or when a thread is made
    # END JSON CRAP
    init = 0
    while True:
        #print head[threadid]
        #users, threadUID = head[threadid]
        pwin.border()
        #threadUID = fpc.concat(threadUID)
        if init == 0:
            numpost = len(fthr.countthreads('/home/{0}/SCHEDULING/p/{1}'.format(username, threadUID)))
            string = str(threadUID)
            pwin.addstr(0, 33, string)
            pwin.move(1, 1)
            pwin.refresh()
            a, c = c[1]
            filename = '/home/{0}/SCHEDULING/p/{1}/{2}.TXT'.format(username, str(threadUID), str(numpost)+'-'+str(threadUID))
            post = open(filename, 'rb')
            b = str(random.randint(0, 257))
            b = b
            if os.path.exists('/home/{0}/SCHEDULING/{1}'.format(username, b)) == False:
                os.makedirs('{0}'.format(b))
                filename = '/home/{0}/SCHEDULING/{1}/{2}.TXT'.format(username, b, str(threadUID))
                z = gpg.decrypt_file(post, output=filename, always_trust=True)
                time.sleep(.25)
            if z.ok:
                posti = open(filename, 'rb')
                postin = posti.readlines()
                posti.close()
            else:
                postin = 'ooooOOOOOoooOimaGHOAST'
            linenum = 0
            for i in range(len(postin)-1):
                linetoadd = postin[i]
                linetoadd.rstrip('\n')
                #print linetoadd
                pwin.addstr(linenum, 0, linetoadd, 0)
                linenum += 1
            post.close()
            pwin.border()
            pwin.addstr(0, 33, string)
            os.remove(filename)
            os.rmdir('/home/{0}/SCHEDULING/{1}'.format(username, b))
            pwin.refresh()
            init = 1

        _ = pwin.getch()

        if _ == curses.KEY_DOWN:
            numposts = len(fthr.countthreads(DIR='/home/{0}/SCHEDULING/p/{1}'.format(username, str(threadUID))))
            numpost = postnum + 1
            if 1 <= numpost <= numposts:
                filename = '/home/{0}/SCHEDULING/p/{1}/{2}.TXT'.format(username, str(threadUID), str(numpost)+'-'+str(threadUID))
            else:
                numpost = postnum - 1
                filename = '/home/{0}/SCHEDULING/p/{1}/{2}.TXT'.format(username, str(threadUID), str(numpost)+'-'+str(threadUID))
            if os.path.isfile(filename):
                #print filename
                post = open(filename, 'rb')
                z = random.randint(0, 257)
                filenamed = '/home/{0}/SCHEDULING/{1}/{2}.TXT'.format(username, str(z), str(numpost)+str(threadUID))
                os.makedirs('/home/{0}/SCHEDULING/{1}/'.format(username, str(z)))
                postd = open(filenamed, 'w+')
                postd.close()
                time.sleep(.5)
                postd = open(filenamed, 'rb')
                #v = gpg.verify_file(post)
                #if v != True:
                #    stdscr.addstr(2, 0, "****WARNING, SIGNATURE IS INVALID****")
                d = gpg.decrypt_file(post, output=filenamed, always_trust=True)
                time.sleep(.5)
                stdscr.move(2,0)
                stdscr.clrtoeol()
                stdscr.refresh()
                post.close()
                postd.close()
                postd = open(filenamed, 'r')
                postin = postd.readlines()
                string = str(d.ok)
                win(2, 0, string)
                stdscr.refresh()
                time.sleep(1)
                stdscr.move(2, 0)
                stdscr.clrtoeol()
                stdscr.refresh()
                string = str(d.status)
                win(2, 0, string)
                stdscr.refresh()
                time.sleep(1)
                stdscr.move(2, 0)
                stdscr.clrtoeol()
                stdscr.refresh()
                linenum = 0
                pwin.refresh()
                #pwin.clear()
                for i in range(len(postin)-1):
                    linetoadd = postin[i]
                    #linetoadd = postin[i]
                    linetoadd.rstrip()
                    #print linetoadd
                    pwin.addstr(linenum, 0, linetoadd, 0)
                    linenum += 1
                postd.close()
                pwin.border()
                pwin.refresh()
                pwin.addstr(0, 33, string)
                postnum += 1
                os.remove(filenamed)
                os.rmdir('/home/{0}/SCHEDULING/{1}/'.format(username, str(z)))
                pwin.move(1, 1)
                pwin.refresh()
        if _ == curses.KEY_F5:
            win(2, 0, "Importing thread...")
            stdscr.refresh()
            time.sleep(2)
            stdscr.move(2, 0)
            stdscr.clrtoeol()
            stdscr.refresh()
        if _ == curses.KEY_F12:
            win(2, 0, "Exiting thread...")
            stdscr.refresh()
            time.sleep(2)
            stdscr.move(2, 0)
            stdscr.clrtoeol()
            stdscr.refresh()
            pwin.clear()
            pwin.refresh()
            return threadUID
        if _ == curses.KEY_F10:
            win(2, 0, "Going to Post Editor...")
            stdscr.refresh()
            #print "Returning to Thread Explorer..."
            time.sleep(2)
            stdscr.move(2, 0)
            stdscr.clrtoeol()
            stdscr.refresh()
            DIR = '/home/{0}/SCHEDULING/p/{1}'.format(username, threadUID)
            postnum = len(os.listdir(DIR))+1
            threadUID = postwriter.postwriter(0, threadid, postnum, threadUID)
            pwin.move(1, 1)





    threadwin.keypad(0)
    postwin.keypad(0)
    curses.nocbreak()
    stdscr.keypad(0)
    curses.echo()
    curses.endwin()
