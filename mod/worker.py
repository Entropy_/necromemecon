#!/usr/bin/env python

import time
import sys
import zmq
import os
import gnupg
#TODO   Add keyrings for each thread

username = os.getlogin()
print username
gpg = gnupg.GPG(gnupghome='/home/{0}/.SCHEDULING'.format(username), use_agent=True, keyring='ringo',
secret_keyring='rings')
# List the keys we have
public_keys = gpg.list_keys()
context = zmq.Context()

# Socket to recieve messages on
receiver = context.socket(zmq.PULL)
receiver.connect("tcp://127.0.0.1:5556")

# Socket to send messages to
sender = context.socket(zmq.PUSH)
sender.connect("tcp://127.0.0.1:5558")

# Socket to inform ventilator about # of workers
worker = context.socket(zmq.PUSH)
worker.connect("tcp://127.0.0.1:5555")

def find(name, path):
    for root, dirs, files in os.walk(path):
        if name in files:
            return os.path.join(root, name)


#TODO   Send message to indicate worker is ready
# Process tasks forever
while True:
        worker.send(b'_')
        s = receiver.recv()
        print ("Received %s threadname." %(s))

        # Simple progress indicator for the viewer
        sys.stdout.write('.')
        sys.stdout.flush()

        #TODO   check for messages that match the requested
        #       thread header
        #TODO   find and replace hermes with *user
        SCHEDULINGthr = find(s, '/home/{0}/SCHEDULING/thr/'.format(username))
        print SCHEDULINGthr
        stream = open(SCHEDULINGthr, "r")
        thr_keys = gpg.list_keys()
        SCHEDULINGthren = gpg.encrypt_file(stream, 'Fred@home.com')
        time.sleep(int(1)*0.001)
        # Send results to sink
        sender.send(str(SCHEDULINGthren))
        #sender.send(b'')
