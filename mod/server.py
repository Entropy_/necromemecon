#!/usr/bin/env python
import time
import os
import zmq
#import gnupg
import zmq
import sys
import threading
import time
import fthr
from random import randint, random
import socket as sock
s = sock.socket(sock.AF_INET, sock.SOCK_DGRAM)
s.connect(("127.0.0.1", 80))
print(s.getsockname()[0])
s.close()
inputs = False
#Keys for gnupg
username = os.getlogin()
#gpg = gnupg.GPG(gnupghome='/home/{0}/.SCHEDULING/'.format(username), use_agent=True, keyring='ringo', secret_keyring='rings')
#save for later, see if errors pop up
#gpg.encoding = 'utf-8'

def tprint(msg):
    """like print, but won't get newlines confused with multiple threads"""
    sys.stdout.write(msg + '\n')
    sys.stdout.flush()



class ServerTask(threading.Thread):
    """ServerTask"""
    def __init__(self):
        threading.Thread.__init__ (self)

    def run(self):
        context = zmq.Context()
        frontend = context.socket(zmq.ROUTER)
        frontend.bind('tcp://*:5570')
        #middleman = context.socket(zmq.DEALER)
        #middleman.bind('tcp://127.0.0.1:5560')
        getnew = context.socket(zmq.ROUTER)
        getnew.bind('ipc://*:5573')

        #backman = context.socket(zmq.ROUTER)
        #backman.bind('inproc://*:5560')
        backend = context.socket(zmq.DEALER)
        backend.bind('inproc://127.0.0.1:5570')

        workers = []
        init = 0
        while True:
            #newworker = getnew.recv_string()
            worker = ServerWorker(context)
            worker.start()
            workers.append(worker)
            print workers
            if init == 0:
                zmq.proxy(frontend, backend)

        #zmq.proxy(backman, middleman)


        #frontend.close()
        #backend.close()
        #context.term()

class ServerWorker(threading.Thread):
    """ServerWorker"""
    def __init__(self, context):
        threading.Thread.__init__ (self)
        self.context = context

    def run(self, iptobindto=0):
        worker = self.context.socket(zmq.DEALER)
        worker.connect('inproc://127.0.0.1:5570')
        outsocket = self.context.socket(zmq.DEALER)
        outsocket.bind('tcp://{0}:5560'.format(iptobindto))
        tprint('Worker{0} started'.format(iptobindto))
        while True:
            threadid = fthr.getheaders()
            if len(fthr.countthreads()) == 0:
                threadid = []
                threadid.append('IMA-GHOAST')
            #print threadid
            numthreads = fthr.countthreads()
            if len(fthr.countthreads()) == 0:
                numthreads = []
                numthreads.append('IMA-GHOAST')
            threads = fthr.processhead()
            if len(fthr.processhead()) == 0:
                threads = []
                threads = [('IMA-GHOAST')]
            msg = worker.recv()
            tprint('Worker{0} received {1}'.format(iptobindto, msg))
            msg = msg.split('--')
            if msg[0] == 'GETPOS':
                outsocket.send_string('STARTPOS')
                tprint("Worker sent STARTPOS")
                threadpos = fthr.getpos()
                sendpos = ''
                for x in threadpos:
                    y, z = x
                    sendpos += str(y)+','+str(z)+','
                sendpos.encode('ascii')
                outsocket.send_string(sendpos)
                tprint("Worker sent {0}".format(sendpos))
                #time.sleep(1)
                outsocket.send_string('255')
                tprint("Worker sent ENDPOS")

#                for x in threadpos:
#                    y, z = x
#                    y = str(y).encode('ascii')
#                    z = str(z).encode('ascii')
#                    outsocket.send_string(y)
#                    time.sleep(.25)
#                    tprint('Worker sent {0}'.format(y))
#                    #time.sleep(.5)
                    #outsocket.send_string(z)
                    #time.sleep(.25)
                    #tprint('Worker sent {0}'.format(z))
                    #time.sleep(.5)
#                outsocket.send_string('255')
#                tprint("Worker sent ENDPOSY")
#                for x in threadpos:
#                    y, z = x
#                    y = str(y).encode('ascii')
#                    z = str(z).encode('ascii')
                    #outsocket.send_string(y)
                    #time.sleep(.25)
                    #tprint('Worker sent {0}'.format(y))
                    #time.sleep(.5)
#                    outsocket.send_string(z)
#                    time.sleep(.25)
#                    tprint('Worker sent {0}'.format(z))
                    #time.sleep(.5)
#                outsocket.send_string('255')
#                tprint("Worker sent ENDPOSX")
            if msg[0] == 'GET':
                if msg[1] == 'DATAPATH':
                    threadUID = msg[2]
                    tprint('Worker sending thread {0}'.format(threadUID))
                    headerpath = '/home/{0}/SCHEDULING/h/{1}'.format(username, threadUID)
                    filepath = '/home/{0}/SCHEDULING/p/{1}'.format(username, threadUID)
                header = open('{0}'.format(headerpath), 'r')
                heads = header.read()
                time.sleep(.21)
                #outsocket.send('BEGINHEADER')
                #tprint("Worker sent BEGINHEADER")
                outsocket.send(heads)
                tprint("Worker sent \n")
                print heads
                time.sleep(.21)
                #outsocket.send("ENDHEAD")
                #tprint("Worker send ENDHEAD")
                count = 0
                posts = fthr.countposts(threadUID)
                print(posts)
                print(len(posts))
                for p in range(len(posts)):
#                    if p != 0:
                    count += 1
                    num = str(count)+'-'
                    outsocket.send_string('BEGINPOST')
                    tprint("Worker sent BEGINPOST")
                    time.sleep(.21)
                    outsocket.send_string(str(len(posts)))
                    tprint("Worker sending post number {0}".format(str(len(posts))))
                    time.sleep(.21)
                    if posts[p] != 0:
                        postin = open('/home/{0}/SCHEDULING/p/{1}/{2}'.format(username, threadUID, str(posts[p])), 'r')
                        line = postin.read()
                        time.sleep(.21)
                        #outsocket.send(str(len(line)))
                        #tprint("Worker sending message length")
                        outsocket.send(line)
                        tprint("Worker sent: \n")
                        time.sleep(.21)
                        tprint(line)
                        postin.close()
                        endpost = "ENDPOST"
                        outsocket.send(endpost)
                        time.sleep(.21)
                        tprint("Sent ENDPOST")
                time.sleep(.5)
                outsocket.send("ENDPOSTS")
                tprint("Sent ENDPOSTS")




            if msg[0] == 'NEW':
                if msg[1] == 'DATAPATH':
                    threadUID = msg[2]
                    postname = msg[3]
                    tprint('Worker making thread {0}'.format(threadUID))
                    filepath = '/home/{0}/SCHEDULING/p/{1}'.format(username, threadUID)
                    headerpath = '/home/{0}/SCHEDULING/h/{1}'.format(username, threadUID)
                    if os.path.exists(filepath) == False:
                        os.mkdir(filepath)

                line = False
                head = open(headerpath, 'w+')
                tprint("Getting headers")
                garbage = worker.recv()
                garbage = worker.recv()
                garbage = worker.recv()
                header = worker.recv()
                print "Got \n"
                print header
                head.write(header)
                head.close()
                postout = open('/home/{0}/SCHEDULING/p/{1}/{2}'.format(username, threadUID, postname), 'w+')
                #tprint("Getting length of file")
                x = True
                #garbage = worker.recv()
                #garbage = worker.recv()
                #garbage = worker.recv()
                #lengthfile = worker.recv()
                #print lengthfile
                #lengthfile = int(lengthfile)
                #tprint(lengthfile)
                x = 0
                #while x < lengthfile:
                garbage = worker.recv()
                garbage = worker.recv()
                garbage = worker.recv()
                try:
                    msg = worker.recv()
                    tprint('Worker -creating thread- received {0}'.format(msg))
                    postout.write(msg)
                    x += 1
                except:
                    debug = 'not a thingy'
                    tprint(debug)
                x = 0
                postout.close()
            #replies = randint(0,4)
            #for i in range(replies):
            #    time.sleep(1. / (randint(1,10)))
            #    worker.send_multipart([ident, msg])

        worker.close()

def main():
    """main function"""
    server = ServerTask()
    server.start()
    server.join()

if __name__ == "__main__":
    main()
