#!/usr/bin/env python
import fpcc as fpc
import os
import json
import random
username = os.getlogin()

def countthreads(DIR='/home/{0}/SCHEDULING/p/'.format(username)):
    threads = os.listdir(DIR)
    return threads
def countposts(thread):
    DIR = '/home/{0}/SCHEDULING/p/{1}'.format(username, thread)
    headers = os.listdir(DIR)
    return headers
def getheaders():
    DIR = '/home/{0}/SCHEDULING/p/'.format(username)
    headers = os.listdir(DIR)
    return headers
def threadhead(threadsh):
    y = 0
    c = []
    for x in threadsh:
        c.append(fpc.concat(x))
        y += 1
    y = 0
    return c

def processhead(init=0):
    DIR = '/home/{0}/SCHEDULING/p/'.format(username)
    headers = os.listdir(DIR)
    headers = threadhead(headers)
    return headers
def getpos():
    threadpos = []
    num = countthreads()
    for x in range(len(num)):
        threadpos.append((random.randint(1, 18), random.randint(1, 50)))
    return threadpos
