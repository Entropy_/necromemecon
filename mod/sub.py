#!/usr/bin/env python

import zmq
import gnupg
import os
username = os.getlogin()
gpg = gnupg.GPG(gnupghome='/home/{0}/.SCHEDULING/'.format(username), use_agent=True, keyring='ringo', secret_keyring='rings')



context = zmq.Context()
inputs = False
#socket to talk to server
print("Co-necting")
outsocket = context.socket(zmq.REQ)
outsocket.connect("ipc://127.0.0.1:5555")

#TODO 10KB seems like a sane number for message size.



# do 10 requests, waiting each time for a response
for request in range(1):
    print ("Sending request to %s ..." % request)
    outsocket.send_string(ascii_armored_public_keys)

    #  Get the reply
    message = outsocket.recv()
    print("Recieved reply %s [ %s ]" % (request, message))
    print "Importing keys"
    import_result = gpg.import_keys(message)
    public_keys = gpg.list_keys()
    print " %s These are the keys we have, we have." % (import_result.fingerprints)
#get all keys from the thread
