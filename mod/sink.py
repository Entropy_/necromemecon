#!/usr/bin/env python

import zmq
import gnupg
import sys
import time
import os

context = zmq.Context()
# Get the keyring
#TODO   Add keyrings for each thread

username = os.getlogin()
gpg = gnupg.GPG(gnupghome='/home/{0}/.SCHEDULING'.format(username), use_agent=True, keyring='ringo',
secret_keyring='rings')
# Socket to receive messages on
receiver = context.socket(zmq.PULL)
receiver.bind("tcp://127.0.0.1:5558")

# File finder
def find(name, path):
    for root, dirs, files in os.walk(path):
        if name in files:
            return os.path.join(root, name)



# Wait for start of batch
s = receiver.recv()
print(s)

# Get the length of the thread
t = receiver.recv()
print(t)

# Get the thread header
h = receiver.recv()

# Start our clock now
tstart = time.time()

# Find and open a file for writing
SCHEDULINGthr = find(h, '/home/{0}/SCHEDULING/thrr/'.format(username))
print SCHEDULINGthr
stream = open(SCHEDULINGthr, "w")



# Process confirmations
for task_nbr in range(int(t)):
    s = receiver.recv()
    decrypted_thr = gpg.decrypt(s)
    stream.write(str(decrypted_thr))
    stream.close()
    sys.stdout.write(':')
    print(decrypted_thr)
    sys.stdout.flush()




# Calculate and report duration of batch
tend = time.time()
print("Total elapsed time: %d msec" % ((tend-tstart)*1000))
