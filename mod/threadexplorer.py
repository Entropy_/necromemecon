#!/usr/bin/env python

import curses
import os
import time
import sys
import fpcc as fpc
import binascii
import fthr
import postwriter
import post
import gnupg
import threading
username = os.getlogin()
gpg = gnupg.GPG(gnupghome='/home/{0}/.SCHEDULING/'.format(username), use_agent=True, keyring='ringo',
secret_keyring='rings')
class ThreadWindow():
    def run(init=0, threadpos=[], threadUID=0):

        threadpos = fthr.getpos()
        if len(fthr.getpos()) == 0:
            threadpos.append((1, 1))
        user = 'owlet@treehole.com'
        stdscr = curses.initscr()
        curses.noecho()
        curses.cbreak()

        stdscr.keypad(1)
        # Init the postwriter
        if init == 1:
            context = zmq.Context()
            socket = context.socket(zmq.DEALER)
            identity = u'clientin-%d' % username
            socket.identity = identity.encode('ascii')
            socket.connect('tcp://127.0.0.1:5571')

        # Thread explorer
        stdscr.addstr(37,96, "HOME Peruses a thread")
        stdscr.addstr(39,96, "INSERT creates a new thread")
        stdscr.addstr(41,96, "F12 exits")
        #stdscr.addstr(39,96, "F5   Imports a thread")
        #stdscr.addstr(41,96, "F2   Bans a user from seeing you")
        #stdscr.addstr(43,96, "F3   Shadowbans a user")
        #stdscr.addstr(45,96, "F6   Bans a user from posting in your threads")
        stdscr.refresh()
        threadwin = stdscr.subwin(22, 95, 3, 0)
        threadwin.border()
        threadwin.move(1, 1)
        cursy = 3
        cursx = 1
        threadwin.keypad(1)
        # Show threads
        #thread1 = fpc.concat('DA3986498FFD5777A7F5A273EAAE1660A51A6404')
        #print thread1
        #thread1 = thread1.encode('UTF-8')
        #TODO Get threads from sink.py
        #INitialize if empty
        threadid = fthr.getheaders()
        if len(fthr.countthreads()) == 0:
            threadid = []
            threadid.append('IMA-GHOAST')
        #print threadid
        currentthread = threadpos[0]
        numthreads = fthr.countthreads()
        if len(fthr.countthreads()) == 0:
            numthreads = []
            numthreads.append('IMA-GHOAST')
        threads = fthr.processhead()
        if len(fthr.processhead()) == 0:
            threads = []
            threads = [('IMA-GHOAST')]
        threadUID = threadid[0]
        #if threadUID == 'GHOAST':
        #    stdscr.addstr(0, 2, 'OOOooooOOOOOOooooOOOOO')
        #else:
        #    if len(threadUID) >= 1:
        #        threadUID = str(threadUID).split('-')
        #        threadUID = threadUID[1]
        #    else:
        #        print 'ooooOOOOOoooOimaGHOAST!'
        #print threads
        y = 0
        for x in threads:
            if x != 0:
                threads[y] = binascii.b2a_base64(x)
                y += 1
        y = 0
        #thread1 = binascii.b2a_base64(thread1)
        #threads.append(thread1)
        #threads.append(thread1)



        threadwin.move(1, 1)
        x = 0
        while True:
            if threadUID == 'GHOAST':
                stdscr.addstr(0, 2, 'OOOooooOOOOOOooooOOOOO')
            else:
                if len(threadUID.split('-')) >= 2:
                    threadUID = str(threadUID).split('-')
                    threadUID = threadUID[1]        #print x
            z = 0
            for y in threadpos:
                thy, thx = y
                threadwin.addstr(thy, thx, threads[z])
                z += 1
            z = 0
            ty, tx = threadpos[x]
            threadwin.addstr(ty, tx, threads[x], curses.A_STANDOUT)
            threadwin.move(ty, tx)
            threadwin.border()
            threadwin.refresh()
            #print len(threadpos)
            cursy, cursx = curses.getsyx()

            _ = threadwin.getch()
            if _ == curses.KEY_IC:
                a = gpg.list_keys(user)
                postwriter.postwriter(0, 0, 1, a[1]['fingerprint'], True, threadpos)
            if _ == curses.KEY_BACKSPACE:
                _ = None
            if _ == curses.KEY_ENTER:
                _ = None
            if _ == curses.KEY_HOME:
                #TODO enter the thread
                socket.send_string("GET")
                post.postreader(threadid[x], 1, 'owlet@treehole.com', threadUID)
                #readthread(threadid)
                _ = None
            if _ == curses.KEY_UP:
                _ = None
                cursy, cursx = curses.getsyx()
                cursy = cursy - 3
                cursx = cursx - 1
                if cursy != 0:
                    if x < len(threadpos):
                        x += 1
                        if x >= len(threadpos):
                            x = len(threadpos)-1
                        ty, tx = threadpos[x]
                        currentthread = threadpos[x]
                        threadwin.addstr(ty, tx, threads[x], curses.A_STANDOUT)
                        threadwin.border()
                        threadwin.move(ty, tx)
                        threadwin.refresh()
                        threadUID = threadid[x]
            if _ == curses.KEY_DOWN:
                _ = None
                cursy, cursx = curses.getsyx()
                cursy = cursy - 3
                cursx = cursx - 1
                if cursy != 21:
                    if x < len(threadpos):
                        x -= 1
                        if x < 0:
                            x = 0
                        ty, tx = threadpos[x]
                        threadwin.addstr(ty, tx, threads[x], curses.A_STANDOUT)
                        threadwin.move(ty, tx)
                        threadwin.border()
                        threadwin.refresh()
                        threadUID = threadid[x]
                    else:
                        pass
            if _ == curses.KEY_LEFT:
                _ = None
                cursy, cursx = curses.getsyx()
                cursy = cursy - 3
                cursx = cursx - 1
                if cursx != 0:
                    if x < len(threadpos):
                        x -= 1
                        if x < 0:
                            x = 0
                        ty, tx = threadpos[x]
                        threadwin.addstr(ty, tx, threads[x], curses.A_STANDOUT)
                        threadwin.move(ty, tx)
                        threadwin.border()
                        threadwin.refresh()
                        threadUID = threadid[x]
                    else:
                        pass
            if _ == curses.KEY_RIGHT:
                _ = None
                cursy, cursx = curses.getsyx()
                cursy = cursy - 3
                cursx = cursx - 1
                if cursx != 94:
                    if x < len(threadpos):
                        x += 1
                        if x >= len(threadpos):
                            x = len(threadpos)-1
                        ty, tx = threadpos[x]
                        threadwin.addstr(ty, tx, threadid[x], curses.A_STANDOUT)
                        threadwin.move(ty, tx)
                        threadwin.border()
                        threadwin.refresh()
                        threadUID = threadid[x]
                    else:
                        pass
    #        if _ == curses.KEY_F10:
    #            stdscr.addstr(2, 0, "Returning to Post Editor...")
    #            stdscr.refresh()
    #            #print "Returning to Thread Explorer..."
    #            time.sleep(2)
    #            stdscr.move(2, 0)
    #            stdscr.clrtoeol()
    #            threadwin.border()
    #            stdscr.refresh()
    #            curses.wrapper(postwriter.postwriter(0))
            if _ == curses.KEY_F12:
                stdscr.addstr(2, 0, "Exiting...")
                stdscr.refresh()
                time.sleep(2)
                stdscr.move(2, 0)
                stdscr.clrtoeol()
                stdscr.refresh()
                return
            else:
                #cursy, cursx = curses.getsyx()
                #cursy = cursy - 3
                #cursx = cursx - 1
                #if cursy >= 20:
                    #threadwin.move(cursy-1, cursx)
                    #_ = None
                if _ != None:
                    pass
                    #cursx += 1
                    #threadwin.addch(_)

            thread1 = fpc.concat('DA3986498FFD5777A7F5A273EAAE1660A51A6404')
            #print thread1
            #thread1 = thread1.encode('UTF-8')
            #TODO Get threads from sink.py
            z = 0
    #        thy = 0
    #        thx = 0
    #        for y in threadpos:
    #            thy, thx = y
    #            threadwin.addstr(thy, thx, threads[z])
    #            threadwin.border()
    #            threadwin.refresh()
    #            z += 1
    #        z = 0
            threadwin.addstr(ty, tx, threads[x], curses.A_STANDOUT)
            threadwin.move(ty, tx)
            threadwin.border()
            threadwin.refresh()


        threadwin.keypad(0)
        postwin.keypad(0)
        curses.nocbreak()
        stdscr.keypad(0)
        curses.echo()
        curses.endwin()
